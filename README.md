#Snapper Flexible Web Page / Element Comparison Tool

The aim of this application is a configurable web service to enable comparison of web pages down to selector level.

This utilises casperjs and image magick as dependancies.

So osx installation instructions:

brew install nodejs
brew install phantomjs  
brew install imagemagick
brew install casperjs
npm install -g gulp



##Image Compare commands

compare -fuzz 5% -metric AE -highlight-color SeaGreen ./baseline/http%3A%2F%2Fwww.alfresco.com\ -\ 1024x768.png ./latest/http%3A%2F%2Fwww.alfresco.com\ -\ 1024x768.png ./difference3A%2F%2Fwww.alfresco.com\ -\ 1024x768.png


#Commands

##Create Baseline Set of Screenshots

` ./app-task.js --config [config-file-path] --task baseline

##Create Latest Set of Screenshots

` ./app-task.js --config [config-file-path] --task latest

##Compare Latest to Baseline

` ./app-task.js --config [config-file-path] --task compare



#Web Server Structure

One Page Application with React JS, all data served via API.

Start webserver with the following command:

DEBUG=myapp npm start

## Server API

get - config file listing


##To Do

1. Requires Web Server - Express for API Commands
2. Requires View Layer - React JS