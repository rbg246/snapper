//Controller for getting data for available processes

var fs = require('fs');
var  _ = require('lodash');

//configs for app
var a = require('../app-config');


module.exports = function (req, res) {
	var rd = [];
	fs.readFile(process.cwd() + '/app-data/base.json', 'utf8', function (err, file) {
		if (err) {
			res.status(500).json({error: 'Config File not Found'});
		} else {
			res.send(file); //file already json
		}
	});
};