//Returns config file for a process
var a = require('../app-config');
var _ = require('lodash');
var fs = require('fs');

module.exports = function (req, res) {
	var rd = [],
		processPath = a.config.process;
	if (req.params.id !== undefined) {
		fs.readFile(a.config.process + req.params.id + '/config.json', 'UTF-8', function (err, file) {
			if (err) {
				res.status('400').json({error : err.message});
			} else {
				res.send(file);
			}
		});
	} else {
		res.status('400').json({error : 'Invalid request - No ID'});
	}
};