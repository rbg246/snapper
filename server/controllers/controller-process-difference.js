//Get the activity log file if one exists and returns JSON from it for rendering

var fs = require('fs');
var  _ = require('lodash');

//configs for app
var a = require('../app-config');

module.exports = function (req, res) {
	var id = req.params.id;
	fs.readFile(a.config.process + id + '/activity.json', 'UTF-8',function (err, file) {
		if (err) {
			res.status('400').json({error : err.message});
		} else {
			res.send(file);
		}
	});
};