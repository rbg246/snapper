//Basic Testing - checks to see if api is working as expected

//Server Setup
var debug = require('debug')('server');
var fs = require('fs');
var api = 'http://localhost:3000/api/';


var should = require('should');
var request = require('superagent');

	describe('API Tests', function () {
		describe('/config [method=get]', function () {
			it ('should return array with correct object and no error', function (done) {
				request.get(api + 'config/').end(function (err, d) {
					// d = JSON.parse(d);
					d = d.body;
					should.exist(d);
					should.not.exist(d.error);
					should.not.exist(err);
					if (d.length > 0) {
						d[0].should.have.property('name');
						d[0].should.have.property('description');
						d[0].should.have.property('id');
					}
					done();
				});
			});
		});
		describe('/base [method=get]', function () {
			it ('should return correct base application data', function (done) {
				var test = JSON.stringify(require(__dirname + '/../app-config.js').base);
				request.get(api + 'base/').end(function (err, d) {
					d = JSON.stringify(d.body);
					d.should.equal(test);
					done();
				});
			});
		});
		describe('process/:id/config [method=get]', function () {
			it('should return correct configuration file', function (done) {
				var configPath = __dirname + '/../app-config/app/';
				fs.readdir(configPath, function (err, files) {
					// body
					if (err) throw (err);
					var expected = require(configPath + files[0]);
					request
						.get(api + 'process/' + expected.id + '/config')
						.end(function (err, res) {
							var body =  JSON.parse(res.text);
							if (err) throw err;
							should.not.exist(err);
							should.exist(body.name);
							body.id.should.equal(expected.id);
							body.name.should.equal(expected.name);
							done();
						});
				});
			});
		});
		describe ('process/:id/difference', function () {
			var configPath = __dirname + '/../app-config/app/';
			it ('should return an error object', function (done) {
				request
					.get(api + 'process/rubbish-process-id/difference')
					.end(function (err, res) {
						var res = JSON.parse(res.text);
						should.exist(err);
						res.error.should.exist;
						done();
					});
			});
			it ('should return correct configuration file', function (done) {
				request
					.get(api + 'process/test-setup-2' + '/difference')
					.end(function (err, res) {
						res.should.exist;
						res.error.should.be.undefined;
						res.should.be.array;
						done();
					});
			});
		});
	});





