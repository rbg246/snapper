var _ = require('lodash');
var fs = require('fs');
var argv = require('yargs').argv;
var rmdirAsync = require('./lib/rmdirAsync');
var compare = require('./lib/compareImages');
var Wrapper = require('array-wrapper');
var mkdirp = require('mkdirp');
var getDataStructure = require('./lib/directories');
var createThumbnails = require('./lib/createThumbnails');
//Lib
var createFileArray = require('./lib/createFileArray');

var appConfig = require(argv.config);
var dir = getDataStructure(appConfig);

//Process Counter
var processCount = 0;
var processTotal =require('./lib/app-process-count.js')(appConfig);

// Setup Direcotries and images
var differenceDirectory = dir.differenceDirectory;

var baselineImages = dir.baselineImages; //baseline images to iterate through
var latestImages = dir.latestImages;
var results = []; //object or array of results


rmdirAsync (differenceDirectory, function (err) {
	if (err) console.log(err);
	mkdirp(differenceDirectory, function(err) {
		var union = _.union(latestImages, baselineImages);
		var intersection = _.intersection(latestImages, baselineImages);
		var missingLatest = _.difference(baselineImages, intersection);
		var missingBaseline = _.difference(latestImages, intersection);
		var data = {
			missingLatest : missingLatest,
			missingBaseline : missingBaseline,
			latestImages : latestImages.length,
			baselineImages : baselineImages.length,
			intersection : intersection.length,
			union : union.length,
		};
		intersection = new Wrapper (intersection);

		var completeCallback = function () {
			fs.writeFile(dir.base + appConfig.id + '/activity.json', JSON.stringify(results), function (err) {
				if (err) {
					throw err;
				} else {
					console.log('completing image processing, file written saved to: ' + dir.base + appConfig.id + '/activity.json');
				}
			});
		};		

		var compareCallback = function (images) {
			var cb = function (data, appConfig) {
				var fn,
					arr;
				if (data !== undefined && data.filename !== 'thumbs') {
					arr = new Buffer(data.filename.replace('.png', ''), 'base64').toString('ascii').split('|');
					fn = {
						url : decodeURI(arr[0]),
						selector : arr[1],
						viewport : arr[2]
					};
					data.data = fn;
					data.thumbs = {
							'baselineImage' : '/app-data/test-setup-2/baseline/thumbs/' + data.filename,
							'latestImage' : '/app-data/test-setup-2/baseline/thumbs/' + data.filename,
							'differenceImage' : '/app-data/test-setup-2/baseline/thumbs/' + data.filename
					};
					results.push(data);
				}
				processCount++;
				console.log(processCount + ' of ' + processTotal + ' processes completed');
				if (data !== undefined) {
					createThumbnails(images.get(), appConfig);
				}
				if (images.next()) {
					compare(images.get(), cb, appConfig);
				} else {
					completeCallback();
				}
			};
			compare (images.get(), cb, appConfig);
		};
		//Bit of an anti-pattern, making thumbnail directories
		mkdirp(differenceDirectory + 'thumbs/', function(err) {
			mkdirp(dir.latestDirectory + 'thumbs/', function(err) {
				mkdirp(dir.baselineDirectory + 'thumbs/', function(err) {
					compareCallback(intersection);
				});
			});
		});
	});
});