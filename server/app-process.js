//Package Dependanceis
var _ = require('lodash');
var fs = require('fs');

//Config
var config = require('./app-config/casper/config');
var appConfig;
var pages;

var directory;

//Process Counter
var processCount = 0;
var processTotal;

//Instantiation
var casper = require('casper').create(JSON.stringify(config));

var setDirectory = function (task) {
	var dir = appConfig.config.directories;
	dir.id = appConfig.id;
	//make base directory if necessary
	fs.makeDirectory(dir.base + dir.id);
	return (dir[task] !== undefined) ? dir.base + dir.id + '/' + dir[task] : undefined;
};

casper.start();
console.log('Starting Process: ');
if (casper.cli.has('task')) {
	// casper.log('App Config: ' + casper.cli.get('app-config'));
	appConfig =  (casper.cli.has('app-config')) ? casper.cli.get('app-config') : undefined;
	if (appConfig === undefined) {
		casper.log('Config not found - exiting', 'error');
		casper.exit(8);
	} else {
		appConfig = require(appConfig);
		processTotal =require('./lib/app-process-count.js')(appConfig);
	}
	pages = appConfig.pages;
	// console.log('CasperJS running ' + casper.cli.get('task'));
	// directory += casper.cli.get('task') + '/';
	directory = setDirectory(casper.cli.get('task'));
	if (directory !== undefined) {
		// console.log('directory', directory);
		if (!fs.isDirectory(directory)) {
			if (fs.makeDirectory(directory)) {
				runCasper();
			} else {
				// casper.log('task directory set ' + directory, 'info');				
				// runCasper();
			}
		} else {
			runCasper();
		}
	} else {
		casper.log('require valid task to be defined', 'error');
		casper.exit();
	}
} else {
	casper.log('require task to be defined via args - casperjs test.js --task=baseline', 'error');
	casper.exit(); //reqire
}

function runCasper () {
	casper.eachThen(pages, function (response) {
		var url = response.data.url;
		var viewports = response.data.viewports;
		var targets = response.data.targets;
		casper.eachThen(viewports, function (res) {
			var viewport = res.data;
			// casper.log('width : ' + viewport.width + ' height : ' + viewport.height, 'debug');
			casper.viewport(viewport.width, viewport.height).thenOpen(url, function (response) {
				var hasSelector = false;
				_.forEach(targets, function (target) {
					processCount++;
					console.log(processCount + ' of ' + processTotal + ' processes completed');
					// url = url.replace('http://', '').replace('https://');
					if (target === 'page' || target === 'document') {
						casper.capture(directory + btoa(url + '|page|' +  viewport.width + 'x' + viewport.height) + '.png');
					} else {
						try {
							casper.captureSelector(directory + btoa(url + '|' + target + '|' +  viewport.width + 'x' + viewport.height) + '.png', target);
						} catch (e) {
							casper.log('No selector ' + '(' + target  + ') found at ' + url,'error');
						}
					}
				});
				console.log(url + '-' +  viewport.width + 'x' + viewport.height);
			});
		});
	});
	casper.run();
}



//compare -fuzz 5% -metric AE -highlight-color SeaGreen ./baseline/http%3A%2F%2Fwww.alfresco.com\ -\ 1024x768.png ./latest/http%3A%2F%2Fwww.alfresco.com\ -\ 1024x768.png ./difference3A%2F%2Fwww.alfresco.com\ -\ 1024x768.png