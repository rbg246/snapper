//return filenames that should be in baseline and latest

var _ = require('lodash');

module.exports = function createFileArray (config) {
	var appConfig = require(config)
		pages = appConfig.pages,
		files = [];
	_.forEach(pages, function (page) {
		var url = page.url;
		_.forEach(page.viewports, function (viewport) {
			_.forEach(page.targets, function (target) {
				if (target === 'page' || target === 'document') {
					files.push(encodeURIComponent(url) + ' -  page - ' +  viewport.width + 'x' + viewport.height+ '.png');
				} else {
					files.push(encodeURIComponent(url) + ' - ' + encodeURIComponent(target) + '-' +  viewport.width + 'x' + viewport.height+ '.png', target);
				}
			});
		});
	});
	return files; 
};