var fs = require('fs');
var gm =  require('gm');


module.exports = function (image, appConfig) {
	// console.log('appConfig - create thumbnails', appConfig);
	var data = require('./directories')(appConfig);
	var cb = function (err) {
		if (err) console.log('err');
	},
	quality = 80;
	gm(data.differenceDirectory + image).thumb('240', '240', data.differenceDirectory + '/thumbs/' + image, quality, cb);
	gm(data.baselineDirectory + image).thumb('240', '240', data.baselineDirectory + '/thumbs/' + image, quality, cb);
	gm(data.latestDirectory + image).thumb('240', '240', data.latestDirectory + '/thumbs/' + image, quality, cb);

};