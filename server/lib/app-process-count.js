// Creates a Total Count of the number of processes to run, 
// facilitating a progress report for processes

//Argument is the appConfig object

var _ = require('lodash');
var processCount = function (config) {
	var pages = config.pages,
		count = 0;
	_.forEach(pages, function (page) {
		// 1 page * number of viewports * number of selectors
		count += 1 * page.viewports.length * page.targets.length; 
	});
	return count;
};

module.exports = processCount;