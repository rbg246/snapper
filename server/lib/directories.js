var fs = require('fs');

function buildDataStructure (appConfig) {
	var data = {};
	var dir = appConfig.config.directories,
		id = appConfig.id;
	//Set Directories from config file and get base images to compare
	if (dir.base !== undefined && dir.difference !== undefined) {
		data.differenceDirectory = process.cwd() + '/' + dir.base + id + '/' + dir.difference;
	} else {
		process.kill();
	}

	if (dir.latest !== undefined) {
		data.latestDirectory = process.cwd() + '/' +  dir.base + id + '/' + dir.latest;
		data.latestImages = fs.readdirSync(data.latestDirectory);
		data.base = dir.base;
	} else {
		console.error('latest images directory not set');
		process.kill();
	}

	if (dir.baseline !== undefined) {
		data.baselineDirectory = process.cwd() +  '/' + dir.base + id + '/' + dir.baseline;
		data.baselineImages = fs.readdirSync(data.baselineDirectory);
	} else {
		process.kill();
	}
	return data;
}


module.exports = buildDataStructure;