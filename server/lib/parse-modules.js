//Helper Function that reads in CommonJS Modules applies a parsing function
//and provides callback
var Wrapper = require('array-wrapper');
//configs for app
var a = require(process.cwd() + '/app-config');

module.exports = function parseModules (files, pf, cb) {
	//if parsefunction not passed
	if (pf === undefined) pf = function (d) {
		return d; //returns data unmodified
	}

	var file = new Wrapper(files),
		rd = [],
		readFile = function (file) {
			//since files are JS modules we can just get them via require
			var d = require(a.configPath + file.get());
			d = pf(d, a.configPath + file.get());
			rd.push(d);
			if (file.next()) {
				readFile(file);
			} else {
				if (cb !== undefined) cb(rd)
			}
		};
	readFile(file);
};