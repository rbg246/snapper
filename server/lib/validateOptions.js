//Basic Validation of the options that gets passed to the Snapper Class

var nested = function (property, obj) {
	if (property.split !== undefined) {
		var property = property.split('.'),
			composite,
			result = true;
		property.forEach(function (index) {
			// console.log(index);
			obj = obj[index];
			if (obj === undefined) result = false;
		});
	} else {
		if (obj[property] === undefined) result = false;
	}
	if (result !== false) result = obj;
	return result;
};


var validate  = function (opts) {
	var expectedProperties = [
			'directories',
			'directories.base',
			'directories.baseline',
			'directories.latest',
			'directories.difference',
			'pages',
		],
		baseMessage = 'Snapper Class Error : Required Setting - %property% not found / check the options you are passing to class';
	if (opts === undefined) {
		return {
			error:true, 
			message : 'Snapper Class Error : Options are undefined, check the options you are passing'
		};
	}
	expectedProperties.forEach(function (property) {
		if (opts[property] === undefined) {
			return {
				error : true, 
				message : baseMessage.replace('%property%', property)
			};
		}
	});
	return {};
};



module.exports = validate;