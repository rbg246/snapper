var spawn = require('child_process').spawn;

processHandler = function () {
  var that = this;
  this.queue = []; //Queue of Processes to carry out

  this.spawn = function (command, args) {
    //queue the command, the queue is an unfinished process queue
    this.queue.push({
      command : command,
      args : args
    });
    console.log('this.queue.length', this.queue.length);
    if (this.queue.length === 1) {
      this.exec(this.queue[0].command, this.queue[0].args);
    }
  }; //Adds a child process to the queue or executes it
  this.logFile = '';
  this.log = function (str) {
    this.logFile += str;
  };
  this.flush = function () {
    this.logFile = '';
  };
  this.broadcast = function () {
    console.log(this.logFile);
    this.flush();
  };
  this.exec = function (command, args) {
    that.cp = spawn(command, args);
    that.cp.stdout.on('data', function (data) {
      that.log('stdout: ' + data);
      console.log('stdout:' + data);
    });
    that.cp.stderr.on('data', function (data) {
      that.log('stderr: ' + data);
      console.log('stdout:' + data);
    });
    that.cp.on('close', function (code) {
      that.log('child process exited with code ' + code);
      that.cp = undefined;
      that.log(that.queue.shift().command + ' has completed');
      if (that.queue.length >= 1) {
        that.exec(that.queue[0].command, that.queue[0].args)
      }
    });
  }; //executes a child process
  return this;
};

module.exports = processHandler;