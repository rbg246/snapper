var fs = require('fs');
var gm =  require('gm');


module.exports = function (image, callback, appConfig) {
	// console.log('appConfig - compareImages', appConfig.config);

	var data = require('./directories')(appConfig);
	// console.log('save dir', data.differenceDirectory + image);
	//hacky fix to ignore the thumbs directory must be a better way
	gm.compare(data.baselineDirectory  + image, data.latestDirectory + image, {
		file : data.differenceDirectory + '/' + image, 
		highlightColor : 'red'
	}, function (err, isEqual, equality, raw) {
		//Saving Directories to relative so that they can be served up through server
		var result = {
			filename : image,
			baselineImage : data.baselineDirectory.replace(process.cwd(), '') + image,
			latestImage : data.latestDirectory.replace(process.cwd(), '') + image,
			difference : equality,
			differenceImage : data.differenceDirectory.replace(process.cwd(), '') + image 
		};
		if (err) callback(undefined, appConfig);
		if (equality > 0) {
			// console.log(data.differenceDirectory + image);
		  	console.log(decodeURIComponent(image) + ' %d\% different to baseline', (equality * 100).toFixed(2));
		}
		// console.log('appConfig - end of compare', appConfig);
	  	callback(result, appConfig);
	});
};