var defaultViewports = [
	{
		width : 320,
		height : 480
	}, 
	{
		width: 1024,
		height: 768
	}, 
	{
		width: 1600, 
		height: 1200
	}
];
module.exports = {
	name : 'Test Setup #2',
	description : 'Second application configuration file, testing 3 pages for various elements',
	id : 'test-setup-2',
	config : {
		directories : {
			base : 'app-data/',
			baseline : 'baseline/',
			latest : 'latest/',
			difference : 'difference/',
		},
		compare : {
			fuzz : 0.1,
			hightlightColor : 'navy' //http://www.graphicsmagick.org/color.html
		},
	},
	pages : [
		{ 
			url : 'http://www.theguardian.com/',
			viewports : defaultViewports,
			targets : ['#header', 'footer', '.facia-page']
		},
		{ 
			url : 'http://www.reddit.com',
			viewports : defaultViewports,
			targets : ['#header', '#siteTable']
		},
		{
			url :'http://www.alfresco.com',
			viewports : defaultViewports,
			targets : ['header', 'document']
		}
	]
};