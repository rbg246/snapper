module.exports = {
	clientScripts : [
		'./node_modules/jquery/dist/jquery.min.js',
		'./node_modules/lodash/index.js'
	],
	viewportSize : {
		width: 1600, 
		height: 1200
	},
	timeout : 50000,
	verbose: true,
    logLevel: 'debug'
};