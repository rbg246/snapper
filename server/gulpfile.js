var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserify = require('browserify');
var watchify = require('watchify');
var babel = require('babelify');
var mocha = require('gulp-mocha');
var sass = require('gulp-sass');
var debug = require('gulp-debug');
var _ = require('lodash');
var fs = require('fs'); 

function compile(watch) {
  var bundler = watchify(browserify('./src/jsx/app.jsx', { debug: true }).transform(babel));

  function rebundle() {
    bundler.bundle()
      .on('error', function(err) { console.error(err); this.emit('end'); })
      .pipe(source('app.js'))
      .pipe(buffer())
      .pipe(sourcemaps.init({ loadMaps: true }))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('./public/js'));
  }

  if (watch) {
    bundler.on('update', function() {
      console.log('-> bundling...');
      rebundle();
    });
  }

  rebundle();
}

function watch() {
  return compile(true);
};

gulp.task('build', function() { return compile(); });
gulp.task('watch', function() { return watch(); });

//Run Mocha tests
gulp.task('run-mocha', function () {
  return gulp.src('./test/test.js', {read : false})
      .pipe(mocha({reporter: 'nyan'}));
});

//Compile Sass & Sourcemaps
gulp.task('run-sass', function () {
  return gulp.src('./src/sass/**/*.scss')
      .pipe(debug({title: 'sass files:'}))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./public/css'));
});

//Gulp Tasks for Running Tasks within Application
gulp.task('watch-app-config', function () {
  gulp.watch(process.cwd() + 'app/config/app/**/*.js', ['cache-app-config']);
});

gulp.task('cache-app-config', function () {
  var path = process.cwd() + '/app-config/app/';
  //Cache Config Files
  fs.readdir(path, function (err, files) {
    var arr = [],
    saveConfig = function (file) {
      var p = './app-data/' + file.id + '/';
      fs.mkdir(p, function () {
        fs.writeFile(p + 'config.json', JSON.stringify(file), 'UTF-8', function (err) {
          if (err) throw err;
        });
      });
      
    };
    _.forEach(files, function (file) {
      var f = require(path + file);
      arr.push({
        name : f.name,
        description : f.description,
        id : f.id
      });
      saveConfig(f);
    });
    fs.writeFile('./app-data/' + 'base.json', JSON.stringify(arr), function (err) {
      if (err) {
        console.log('error in concatenating file - ' + err);
      } else {
        console.log('Data saved to cache');
      }
    });
  });
  return gulp.src('./src/*.js', {read : false});
});

gulp.task('cache-app-data', function () {
  var path = process.cwd() + '/app-config/app/',
    base = process.cwd()  + '/app-data/';
  fs.readdir(path, function (err, files) {
    if (err) throw err;
    _.forEach(files, function (file) {
      var f = require(path + file),
      data = {
        id : f.id,
        dir : f.config.directories
      };
      console.log(base + f.id + '/log.json');
      fs.readFile(base + f.id + '/log.json', 'UTF-8', function (err, data) {
        if (err) {
          console.log(f.id + ' - no processes run yet');
        } else {
          fs.writeFile('./app-data/' + activity + '.json', data, 'UTF-8', function (err){
            if (err) throw err;
            console.log('file saved')
          });
        }

      });
    });
  });

  return gulp.src('./src/*.js', {read : false});
});

//Watch Task
gulp.task('watch2', function () {
  //watch source files
  gulp.watch('./src/sass/**/*.scss', ['run-sass']);

  //watch other js
  gulp.watch('./lib/**.js', ['run-mocha']);
  gulp.watch('./routes/**.js', ['run-mocha']);
  gulp.watch('./controllers/**.js', ['run-mocha']);


});


gulp.task('default', ['watch', 'run-mocha', 'run-sass', 'watch2', 'cache-app-data', 'cache-app-config']);