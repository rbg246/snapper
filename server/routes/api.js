var express = require('express');
var router = express.Router();
var SpawnHandler = require('../lib/spawn-handler');

var spawnHandler = new SpawnHandler ();


//Gets Base Application Config
router.route('/base')
	.get(require('../controllers/controller-base'));

//Get configuration listing, get an individual config file
router
	.route('/config')
	.get(require('../controllers/controller-config'));

router.route('/process/:id/config')
	.get(require('../controllers/controller-config-detail'));

//Processes - baseline
//Need to work out how to manage child processes and pass back information
router
	.route('/process/:id/latest')
	.get (function (req, res) {
		require('../controllers/controller-process-latest')(req, res, spawnHandler);
	});
router
	.route('/process/:id/baseline')
	.get (function (req, res) {
		require('../controllers/controller-process-baseline')(req, res, spawnHandler);
	});

//Processes - get data
router
	.route('/process/:id/difference')
	.get (function (req, res) {
		require('../controllers/controller-process-difference')(req, res);
	});

//Processes - compares images
router
	.route('/process/:id/compare')
	.get (function (req, res) {
		require('../controllers/controller-process-compare')(req, res, spawnHandler);
	});

module.exports = router;