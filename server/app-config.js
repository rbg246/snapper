//File with constants to access application with

/* Important the base property will return data to the user via the api */

module.exports = {
    base: {
        'app_name': 'The Difference Generator',
        'author': {
            name: 'Richard Gaunt',
            email: 'richard@bournegaunt.com'
        },
        menu: {
            start: '/',
            settings: [{
                title: 'link 1',
                href: '/link1'
            }, {
                title: 'link 1',
                href: '/link1'
            }]
        }
    },
    config: {
        base: process.cwd() + '/app-data/base.json',
        process: process.cwd() + '/app-data/'
    },
    configPath: process.cwd() + '/app-config/app/',
};
