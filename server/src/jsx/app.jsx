import React from 'react';
import { Router, Route, Link, IndexRoute } from 'react-router';
import jQuery from 'jquery';
import createBrowserHistory from 'history/lib/createBrowserHistory';
import Slider from 'bootstrap-slider';
import lodash from 'lodash';

var $ = jQuery;
var _ = lodash;
var api = {
    base : '/api/base',
    processes : '/api/config',
    difference : '/api/process/:id/difference',
    processDetail : '/api/process/:id/config'
};

//Header Components

var Header = React.createClass({
    getInitialState: function() {
        return {base: {}};
    },
    componentDidMount: function() {
        $.ajax({
            url: api.base,
            dataType: 'json',
            cache: false,
            success: function(data) {
                if (this.isMounted()) {
                    this.setState({base: data});
                }
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(api.base, status, err.toString());
            }.bind(this)
        });
    },
    render : function () {
        return (
        <nav className="navbar navbar-inverse navbar-embossed navbar-fixed-top">
            <div className="container-fluid">
                <div className="navbar-header">
                    <a className="navbar-brand" href="/">{this.state.base.app_name}</a>
                </div>
                <div className="collapse navbar-collapse">
                    <SettingDropDown menu={this.state.base.menu}/>
                </div>

            </div>
        </nav>
        );
    }
});

var SettingDropDown = React.createClass({
    getInitialState : function () {
        return {
            menu : {},
            dropdown : 'hidden dropdown-menu' 
        };
    },
    toggleMenu : function () {
        var toggle = (this.state.dropdown.includes('hidden')) ? 'dropdown-menu' : 'hidden dropdown-menu';
        this.setState({'dropdown': toggle});
    },
    render : function () {
        var menuItems = (this.props.menu !== undefined) ? this.props.menu.settings : [],
            items = menuItems.map(function (menuItem) {
                return (
                    <li className="dropdown-menu-item">
                        <a className="dropdown-menu-item-link" href="{menuItem.link}">{menuItem.title}</a>
                    </li>
                );
            });
        return (
            <div className="navbar-right dropdown">
                <a href="#" onClick={this.toggleMenu} className="fui-gear dropdown-toggle"></a>
                <ul className={this.state.dropdown}>
                    {items}
                </ul>
            </div>
        );
    }
});

//Home Page Components

var ContentHomePage = React.createClass({
    getInitialState : function () {
        return {
            processes : []
        };
    },
    componentDidMount : function () {
        $.ajax({
            url: api.processes,
            dataType: 'json',
            cache: false,
            success: function(data) {
                if (this.isMounted()) {
                    this.setState({processes: data});
                }
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(api.base, status, err.toString());
            }.bind(this)
        }); 
    },
    render : function () {
        return (
            <div className="container content">
                <div className="row">
                    <h1>Available Processes</h1>
                </div>
                <div className="row">
                    <ProcessPanels panels={this.state.processes} />
                </div>
            </div>
        );
    }
});

var ProcessPanels = React.createClass({
    render : function () {
        var data = (this.props.panels !== undefined) ? this.props.panels : [],
            panels = data.map(function (panel) {
                var href = {
                    difference : '/process/' + panel.id,
                    configure : '/process/' + panel.id + '/configure/'
                };
                return (
                    <div className="col-lg-3">
                        <div className="panel panel-primary ">
                            <div className="panel-heading">
                                <h5>{panel.name}<span className="pull-right"><a className="fui-gear" href={href.configure}></a></span></h5>
                            </div>
                            <div className="panel-body">
                                {panel.description}
                                <a href={href.difference} className="btn btn-block btn-success panel-cta">Differences</a>
                            </div>
                        </div>
                    </div>
                );
            });
        return (
            <div className="row">
                {panels}
            </div>
        );
    }
});

var HomePage = React.createClass({
    render : function () {
        return (
            <div>
                <Header />
                {this.props.children}
            </div>
        );
    }
});

var app = {};

var DifferencePage = React.createClass({
    getInitialState : function () {
        return {
            process : {
                name : '',
                description : ''
            },
            data : []
        };
    },
    componentDidMount : function () {
        const id = this.props.params.id;
        $.ajax({
            url: api.difference.replace(':id', id),
            dataType: 'json',
            cache: false,
            success: function(data) {
                if (this.isMounted()) {
                    this.setState({data: data});
                }
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(api.difference, status, err.toString());
            }.bind(this)
        });
        $.ajax({
            url: api.processDetail.replace(':id', id),
            dataType: 'json',
            cache: false,
            success: function(data) {
                if (this.isMounted()) {
                    this.setState({process: data});
                }
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(api.base, status, err.toString());
            }.bind(this)
        });
    },
    render : function () {
        const id = this.props.params.id;
        var process = this.state.process;
        var data = this.state.data;
        console.log('data', data);
        return (
            <div className="container content">
                <div className="row">
                    <div className="col-lg-12">
                        <h1>
                            {process.name}
                        </h1>
                    </div>
                    <div className="col-lg-12">
                        <p>
                            {process.description}
                        </p>
                    </div>
                    <div className="col-lg-12">
                        <DataPanel panels={data} />
                    </div>

                </div>
            </div>
        );
    }
});

var DataPanel = React.createClass({
    render : function () {
        var data = (this.props.panels !== undefined) ? this.props.panels : [],
            panels = data.map(function (panel) {
                var fn = atob(panel.filename.replace('.png', ''));
                return (
                    <div className="col-lg-4">
                        <div className="panel panel-primary ">
                            {fn}
                        </div>
                    </div>
                );
            });
        return (
            <div className="row">
                {panels}
            </div>
        );
    }
});

React.render(
    <Router history={createBrowserHistory()} location="history">
        <Route  path="/" component={HomePage}>
            <IndexRoute component={ContentHomePage} />
            <Route path="/process/:id" component={DifferencePage} />
        </Route>
    </Router>,
    document.getElementById('app')
);