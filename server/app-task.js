#!/usr/bin/env node
var argv = require('yargs').argv;
var task = argv.task;
var config = argv.config;
var spawn = require('child_process').spawn;


//Child process
var cp;

if (task === undefined) {
	console.error(moment().format() + ' [error] '.red + 'No task defined - ./index.js --task compare | ./index.js --task baseline | ./index.js --task latest');
	process.kill();
}

if (config === undefined) {
	console.error(moment().format() + ' [error] '.red + 'No config defined - ./index.js --task compare | ./index.js --task baseline | ./index.js --task latest');
	process.kill();
}

switch (task) {
	case 'compare' :
		cp = spawn('node', ['app-compare.js', '--task', task, '--config', config]);
		break;
	case 'baseline' :
	case 'latest' :
		// execProcess(task);
		//must be app-config cli option as config is reserved
		cp = spawn('casperjs', ['app-process.js', '--task=' + task, '--app-config=' + config]);
		break;
	default :
		console.error(moment().format() + ' [error] '.red +'Invalid task: \nValid Tasks: compare, baseline, latest');
		process.kill();
}

//Logging Casper Events
var moment = require('moment');
var colors = require('colors');

cp.stdout.on('data', function (data) {
  console.log(moment().format() + ' [log] '.green + data);
});

cp.stderr.on('data', function (data) {
  console.log(moment().format() + ' [error] '.red + data);
});

cp.on('close', function (code) {
  console.log(moment().format() + ' [process] '.yellow + 'child process exited with code ' + code);
});